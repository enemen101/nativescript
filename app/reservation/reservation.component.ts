import { Component, OnInit, ChangeDetectorRef, ViewContainerRef } from '@angular/core';
import { DrawerPage } from '../shared/drawer/drawer.page';
import { TextField } from 'ui/text-field';
import { Switch } from 'ui/switch';
import { Validators, FormBuilder, FormGroup} from '@angular/forms';
import { ModalDialogService, ModalDialogOptions } from "nativescript-angular/modal-dialog";
import { ReservationModalComponent } from "../reservationmodal/reservationmodal.component";
import { View } from 'ui/core/view'; 
import { Page } from 'ui/page'; 
import { SwipeGestureEventData, SwipeDirection } from 'ui/gestures';
import * as enums from 'ui/enums'; 
import { CouchbaseService } from "../services/couchbase.service"; 


@Component({

    selector: 'app-reservation',
    moduleId: module.id,
    templateUrl: './reservation.component.html'
})

export class ReservationComponent extends DrawerPage implements OnInit {

    docID: string = "reservations"; 
    reservations: any[];  

    reservation: FormGroup;

    guests: number;
    smoking: boolean;
    dateTime: string; 

    formIn: View; 
    form: View;
    formOut : boolean = false; 

    constructor(private changeDetectorRef: ChangeDetectorRef,
        private formBuilder: FormBuilder, private modalService: ModalDialogService, 
        private vcRef: ViewContainerRef, private page: Page,
        private couchservice: CouchbaseService) {
            super(changeDetectorRef);

            this.reservation = this.formBuilder.group({
                guests: 3,
                smoking: false,
                dateTime: ['', Validators.required]
            }); 

            this.reservations = [];

            let doc = this.couchservice.getDocument(this.docID); 
            if (doc == null) {
                
                this.couchservice.createDocument({"reservations": this.reservations}, this.docID)
            }
            else {
                this.reservations = doc.reservations; 
                
            }
    }
        
    
    ngOnInit() {

    }

    onSmokingChecked(args) {
        let smokingSwitch = <Switch>args.object;
        if (smokingSwitch.checked) {
            this.reservation.patchValue({ smoking: true });
        }
        else {
            this.reservation.patchValue({ smoking: false });
        }
    }

    onGuestChange(args) {
        let textField = <TextField>args.object;

        this.reservation.patchValue({ guests: textField.text});
    }

    onDateTimeChange(args) {
        let textField = <TextField>args.object;

        this.reservation.patchValue({ dateTime: textField.text});
    }

    onSubmit() {
        console.log(JSON.stringify(this.reservation.value));
        this.animateForm(); 
        this.reservations.push(this.reservation.value);
        this.couchservice.updateDocument(this.docID, {"reservations": this.reservations}); 
        console.log(JSON.stringify(this.couchservice.getDocument(this.docID)));
        
    }

    createModalView(args) {

        let options: ModalDialogOptions = {
            viewContainerRef: this.vcRef,
            context: args,
            fullscreen: false
        };

        this.modalService.showModal(ReservationModalComponent, options)
            .then((result: any) => {
                if (args === "guest") {
                    this.reservation.patchValue({guests: result});
                }
                else if (args === "date-time") {
                    this.reservation.patchValue({ dateTime: result});
                }
            });

    }

    animateForm() {

        this.form = this.page.getViewById<View>("resForm");
        this.formIn = this.page.getViewById<View>("resDet");

        if (this.reservation.valid) {
            this.form.animate({
                scale: {x: 0, y: 0},
                opacity: 0,
                duration: 500
            })
            .then(() => {
                this.formIn.animate({
                    scale: { x: -1, y: -1 },
                    opacity: 0
                })
            })
            .then(() => {
                this.formOut = true; 
                this.formIn.animate({
                    scale: {x: 1, y: 1},
                    opacity: 1,
                    duration: 500
                })
            });
            this.guests = this.reservation.value.guests;
            this.smoking = this.reservation.value.smoking;
            this.dateTime = this.reservation.value.dateTime;

            
            
            
            
        }
    }

}