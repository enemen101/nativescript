import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { Validators, FormBuilder, FormGroup} from '@angular/forms';
import { ModalDialogParams } from "nativescript-angular/modal-dialog";
import { Comment } from "../shared/comment";
import { Slider } from 'tns-core-modules/ui/slider';
import { TextField } from 'ui/text-field';

@Component({
    
        selector: 'app-comment',
        moduleId: module.id,
        templateUrl: './comment.component.html'
})

export class CommentPage implements OnInit{

    commentForm: FormGroup;
    comment: Comment; 
    
    constructor(
        private formBuilder: FormBuilder, private params: ModalDialogParams) {
    
            this.commentForm = this.formBuilder.group({
                rating: 1,
                comment: ['', Validators.required],
                author: ['', Validators.required], 
            }); 
        }
    
    ngOnInit() {

    }
    
        onSliderChange(args) {
            let sliderValue = <Slider>args.object;
            this.commentForm.patchValue({ rating: sliderValue.value });
        }
    
        onAuthorChange(args) {
            let textField = <TextField>args.object;
            this.commentForm.patchValue({ author: textField.text });
        }
    
        onCommentChange(args) {
            let textField = <TextField>args.object;
            this.commentForm.patchValue({ comment: textField.text });
        }
    
    public onSubmit() {
        let comment : Comment = this.commentForm.value;
        comment.date = (new Date()).toISOString()
        this.params.closeCallback(comment); 
        
    }



}